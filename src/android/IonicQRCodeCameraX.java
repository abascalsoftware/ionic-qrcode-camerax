package ionic.qrcode.camerax;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IonicQRCodeCameraX extends CordovaPlugin {
    private CallbackContext callback;
    public static final int INTENT_START = 1;
	private static final String TAG = "IQCX->IonicQRCodeCameraX";

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Context context = cordova.getActivity().getApplicationContext();
        callback = callbackContext;
        if(action.equals("start")){
            Log.d(TAG,"IonicQRCodeCameraX execute... context.getPackageName(): "+context.getPackageName());
            String invalid_format_label = args.getString(0);
            String open_settings_label = args.getString(1);
            String permission_again_label = args.getString(2);
            String permission_again_title = args.getString(3);
            String qrcode_label = args.getString(4);
            String url_prefix = args.getString(5);
            Intent intent = new Intent(context, ScannerBarcodeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("action", IonicQRCodeCameraX.INTENT_START);
            intent.putExtra("invalid_format_label", invalid_format_label);
            intent.putExtra("open_settings_label", open_settings_label);
            intent.putExtra("permission_again_label", permission_again_label);
            intent.putExtra("permission_again_title", permission_again_title);
            intent.putExtra("qrcode_label", qrcode_label);
            intent.putExtra("url_prefix", url_prefix);
            // cordova.setActivityResultCallback (this);
			cordova.startActivityForResult((CordovaPlugin) this, intent, IonicQRCodeCameraX.INTENT_START);
			// startActivity(new Intent(cordova.getActivity(),ScannerBarcodeActivity.class));
            return true;
        }
        return false;
    }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d(TAG,"onActivityResult requestCode: "+Integer.toString(requestCode));
        Log.d(TAG,"onActivityResult resultCode: "+Integer.toString(resultCode));
        switch(requestCode){
            case IonicQRCodeCameraX.INTENT_START:
                if(resultCode == cordova.getActivity().RESULT_OK){
                    try {
                        Bundle extras = new Bundle();
                        if(data != null){
                            extras = data.getExtras();
                        }
                        JSONObject obj = new JSONObject();
                        obj.put("status", extras.getString("status"));
                        obj.put("codigo", extras.getString("codigo"));
                        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, obj);
                        callback.sendPluginResult(pluginResult);
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                }else{
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                    callback.sendPluginResult(pluginResult);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }
}