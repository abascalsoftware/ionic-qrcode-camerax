var exec = require('cordova/exec');

var service = "IonicQRCodeCameraX";

exports.start = function (success, error, params) {
    console.log("IonicQRCodeCameraX.js params: ",params);
    console.log("IonicQRCodeCameraX.js JSON.stringify(params): "+JSON.stringify(params));
    var arrayParams = [];
    arrayParams.push(params.invalid_format_label || "");
    arrayParams.push(params.open_settings_label || "");
    arrayParams.push(params.permission_again_label || "");
    arrayParams.push(params.permission_again_title || "");
    arrayParams.push(params.qrcode_label || "");
    arrayParams.push(params.url_prefix || "");
    exec(success, error, service, 'start', arrayParams);
};
